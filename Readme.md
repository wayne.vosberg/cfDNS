# cfDNS

## What

A simple interface to the Cloudflare DNS api. 

## Why

I needed a way to occasionally update my IPv6 address from a cron job. And I also wanted to play around with Python's [urllib](https://docs.python.org/3/library/urllib.html).

## How

* You must add your Cloudflare username (email) and api key before using any other command.
* cfDNS will lookup up your zone ID and domain(s) with your username/api key.
* Records to update are identified by their ID, which is reported by the dump or lookup commands.
* Update is the *ONLY* command that will attempt to make a change. The others only query existing data.

```bash
$ cfDNS -h
usage: cfDNS [-h] [--domain DOMAIN] {config,zone,dump,lookup,update} ...

Interface to Cloudflare DNS.

positional arguments:
  {config,zone,dump,lookup,update}
                        command help
    config              Set Cloudflare eMail and API Key.
    zone                Get Cloudflare Zone information.
    dump                Dump all Cloudflare DNS information.
    lookup              Lookup DNS record(s) by type, fqdn, or IP address
    update              Update a DNS records IP address by ID.

optional arguments:
  -h, --help            show this help message and exit
  --domain DOMAIN       Select the domain to use, or use the first found.

$ cfDNS config -h
usage: cfDNS config [-h] email apiKey

positional arguments:
  email       Cloudflare User eMail.
  apiKey      Cloudflare API Key.

optional arguments:
  -h, --help  show this help message and exit

$ cfDNS zone -h
usage: cfDNS zone [-h]

optional arguments:
  -h, --help  show this help message and exit

$ cfDNS dump -h
usage: cfDNS dump [-h]

optional arguments:
  -h, --help  show this help message and exit

$ cfDNS lookup -h
usage: cfDNS lookup [-h] search

positional arguments:
  search      record type, fqdn, or IP address

optional arguments:
  -h, --help  show this help message and exit

$ cfDNS update -h
usage: cfDNS update [-h] recordId ipAddress

positional arguments:
  recordId    DNS record ID, from dump/lookup.
  ipAddress   New IP address.

optional arguments:
  -h, --help  show this help message and exit
```
